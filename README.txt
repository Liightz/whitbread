Installation
Assumptions
 - You have maven installed
 - You have java 8 installed

1. Clone repository https://gitlab.com/Liightz/whitbread
2. Go to the directory you cloned into, with the pom.xml
3. Open a command line here
4. Run mvn clean install -P prod
5. Create a new directory
6. Copy the jar from at /target/whitbread-1.0-SNAPSHOT.jar into the new directory
7. Copy the properties file at /src/main/resources/application.properties into the new directory
8. Edit application.properties in the new directory and add your client_id/client_secret
9. In the new folder, open a command line and run: java -jar whitbread-1.0-SNAPSHOT.jar
10. Go to http://localhost:8085/recommended/<location>, where location is where you are looking for (such as London)

Summary:
This API simply calls Four Square's API based on the location provided. It uses the /venues/explore API, as this provides recommended locations in the area
I have added some basic error handling, however I was unsure what we would expect to have returned when foursquare api returns an error, so I have tried to return as close to what it returned as possible

This API simply returns the same data Four Square's API returns, but could easily be reduced by removing some of the domain/foursquare objects

I have assumed we don't need to provide any extra parameters that Four Squares API provides, however this can easily be extended with the current set up

A few things to note:
 1. I tried to use a already build Four Square java API, such as the one recommended by four square (https://github.com/wallabyfinancial/foursquare-api-java),
    however they all seemed to fail reading the Four Square return objects, so I decided to implement this myself (which was a shame)
 2. Four Square doesn't seem to recommend all that many locations when searching by name, such as moorgate. It most likely does better when provided long/latitude,
    so if I was to need to implement this, I would most likely call another API with the location name, to get a long/lat, and use this against the four square API to get better results
