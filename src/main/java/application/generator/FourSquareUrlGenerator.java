package application.generator;

import application.config.Properties;
import application.domain.Request;
import application.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * @author j000106.
 */
@Component
public class FourSquareUrlGenerator {

    private Properties properties;

    @Autowired
    public FourSquareUrlGenerator(Properties properties) {
        this.properties = properties;
    }

    public URI generateExploreUrl(Request request) throws URISyntaxException {
        URI uri = createUrl("/venues/explore");
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUri(uri);
        uriBuilder.queryParam("near", request.getSearchLocation());
        return uriBuilder.build().encode().toUri();
    }

    private URI createUrl(String specificUrl) {
        String baseUrl = Constants.FOURSQUARE_BASE_URL;
        baseUrl += specificUrl;
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(baseUrl);
        uriBuilder
                .queryParam("client_id", properties.getFourSquareClientId())
                .queryParam("client_secret", properties.getFourSquareClientSecret())
                .queryParam("v", "20170827"); //Maybe V parameter should be dynamic, it isn't completely obvious

        return uriBuilder.build().toUri();
    }
}
