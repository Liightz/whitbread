package application.validator;

import application.domain.Request;
import application.exception.InvalidRequestException;
import org.springframework.stereotype.Component;

/**
 * @author j000106.
 */
@Component
public class RequestValidator {

    public boolean validate(Request request) throws InvalidRequestException {
        if (request.getSearchLocation() == null || request.getSearchLocation().isEmpty()) {
            throw new InvalidRequestException("You must provide a non empty search location");
        }
        return true;
    }

}
