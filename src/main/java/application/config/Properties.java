package application.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Properties {

    private String fourSquareClientId;

    private String fourSquareClientSecret;

    public String getFourSquareClientId() {
        return this.fourSquareClientId;
    }

    public String getFourSquareClientSecret() {
        return this.fourSquareClientSecret;
    }

    @Value("${foursquare.client.id}")
    public void setFourSquareClientId(String fourSquareClientId) {
        this.fourSquareClientId = fourSquareClientId;
    }

    @Value("${foursquare.client.secret}")
    public void setFourSquareClientSecret(String fourSquareClientSecret) {
        this.fourSquareClientSecret = fourSquareClientSecret;
    }
}
