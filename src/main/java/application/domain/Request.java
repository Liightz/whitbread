package application.domain;

public class Request {

    private String searchLocation;

    public Request(String searchLocation) {
        this.searchLocation = searchLocation;
    }

    public String getSearchLocation() {
        return searchLocation;
    }
}
