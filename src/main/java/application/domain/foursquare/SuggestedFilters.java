package application.domain.foursquare;

import java.util.ArrayList;

public class SuggestedFilters {
    private String header;
    private ArrayList<Filter> filters;

    public String getHeader() { return this.header; }

    public void setHeader(String header) { this.header = header; }

    public ArrayList<Filter> getFilters() { return this.filters; }

    public void setFilters(ArrayList<Filter> filters) { this.filters = filters; }
}