package application.domain.foursquare;

public class Geocode {
    private String what;
    private String where;
    private Center center;
    private String displayString;
    private String cc;
    private Geometry geometry;
    private String slug;
    private String longId;

    public String getWhat() { return this.what; }

    public void setWhat(String what) { this.what = what; }

    public String getWhere() { return this.where; }

    public void setWhere(String where) { this.where = where; }

    public Center getCenter() { return this.center; }

    public void setCenter(Center center) { this.center = center; }

    public String getDisplayString() { return this.displayString; }

    public void setDisplayString(String displayString) { this.displayString = displayString; }

    public String getCc() { return this.cc; }

    public void setCc(String cc) { this.cc = cc; }

    public Geometry getGeometry() { return this.geometry; }

    public void setGeometry(Geometry geometry) { this.geometry = geometry; }

    public String getSlug() { return this.slug; }

    public void setSlug(String slug) { this.slug = slug; }

    public String getLongId() { return this.longId; }

    public void setLongId(String longId) { this.longId = longId; }
}