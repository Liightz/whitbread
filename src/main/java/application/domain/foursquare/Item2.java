package application.domain.foursquare;

public class Item2 {
    private String summary;
    private String type;
    private String reasonName;

    public String getSummary() { return this.summary; }

    public void setSummary(String summary) { this.summary = summary; }

    public String getType() { return this.type; }

    public void setType(String type) { this.type = type; }

    public String getReasonName() { return this.reasonName; }

    public void setReasonName(String reasonName) { this.reasonName = reasonName; }
}