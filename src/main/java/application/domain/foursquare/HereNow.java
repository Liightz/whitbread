package application.domain.foursquare;

import java.util.ArrayList;

public class HereNow {
    private int count;
    private String summary;
    private ArrayList<Object> groups;

    public int getCount() { return this.count; }

    public void setCount(int count) { this.count = count; }

    public String getSummary() { return this.summary; }

    public void setSummary(String summary) { this.summary = summary; }

    public ArrayList<Object> getGroups() { return this.groups; }

    public void setGroups(ArrayList<Object> groups) { this.groups = groups; }
}
