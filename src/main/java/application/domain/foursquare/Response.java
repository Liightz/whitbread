package application.domain.foursquare;

import java.util.ArrayList;

public class Response {
    private SuggestedFilters suggestedFilters;
    private Geocode geocode;
    private Warning warning;
    private String headerLocation;
    private String headerFullLocation;
    private String headerLocationGranularity;
    private int totalResults;
    private SuggestedBounds suggestedBounds;
    private ArrayList<Group> groups;

    public SuggestedFilters getSuggestedFilters() { return this.suggestedFilters; }

    public void setSuggestedFilters(SuggestedFilters suggestedFilters) { this.suggestedFilters = suggestedFilters; }

    public Geocode getGeocode() { return this.geocode; }

    public void setGeocode(Geocode geocode) { this.geocode = geocode; }

    public Warning getWarning() { return this.warning; }

    public void setWarning(Warning warning) { this.warning = warning; }

    public String getHeaderLocation() { return this.headerLocation; }

    public void setHeaderLocation(String headerLocation) { this.headerLocation = headerLocation; }

    public String getHeaderFullLocation() { return this.headerFullLocation; }

    public void setHeaderFullLocation(String headerFullLocation) { this.headerFullLocation = headerFullLocation; }

    public String getHeaderLocationGranularity() { return this.headerLocationGranularity; }

    public void setHeaderLocationGranularity(String headerLocationGranularity) { this.headerLocationGranularity = headerLocationGranularity; }

    public int getTotalResults() { return this.totalResults; }

    public void setTotalResults(int totalResults) { this.totalResults = totalResults; }

    public SuggestedBounds getSuggestedBounds() { return this.suggestedBounds; }

    public void setSuggestedBounds(SuggestedBounds suggestedBounds) { this.suggestedBounds = suggestedBounds; }

    public ArrayList<Group> getGroups() { return this.groups; }

    public void setGroups(ArrayList<Group> groups) { this.groups = groups; }
}
