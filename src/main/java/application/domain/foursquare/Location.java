package application.domain.foursquare;

import java.util.ArrayList;

public class Location {
    private String address;
    private String crossStreet;
    private double lat;
    private double lng;
    private ArrayList<LabeledLatLng> labeledLatLngs;
    private String postalCode;
    private String cc;
    private String city;
    private String state;
    private String country;
    private ArrayList<String> formattedAddress;

    public String getAddress() { return this.address; }

    public void setAddress(String address) { this.address = address; }

    public String getCrossStreet() { return this.crossStreet; }

    public void setCrossStreet(String crossStreet) { this.crossStreet = crossStreet; }

    public double getLat() { return this.lat; }

    public void setLat(double lat) { this.lat = lat; }

    public double getLng() { return this.lng; }

    public void setLng(double lng) { this.lng = lng; }

    public ArrayList<LabeledLatLng> getLabeledLatLngs() { return this.labeledLatLngs; }

    public void setLabeledLatLngs(ArrayList<LabeledLatLng> labeledLatLngs) { this.labeledLatLngs = labeledLatLngs; }

    public String getPostalCode() { return this.postalCode; }

    public void setPostalCode(String postalCode) { this.postalCode = postalCode; }

    public String getCc() { return this.cc; }

    public void setCc(String cc) { this.cc = cc; }

    public String getCity() { return this.city; }

    public void setCity(String city) { this.city = city; }

    public String getState() { return this.state; }

    public void setState(String state) { this.state = state; }

    public String getCountry() { return this.country; }

    public void setCountry(String country) { this.country = country; }

    public ArrayList<String> getFormattedAddress() { return this.formattedAddress; }

    public void setFormattedAddress(ArrayList<String> formattedAddress) { this.formattedAddress = formattedAddress; }
}

