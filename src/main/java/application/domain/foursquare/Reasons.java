package application.domain.foursquare;

import java.util.ArrayList;

public class Reasons {
    private int count;
    private ArrayList<Item2> items;

    public int getCount() { return this.count; }

    public void setCount(int count) { this.count = count; }

    public ArrayList<Item2> getItems() { return this.items; }

    public void setItems(ArrayList<Item2> items) { this.items = items; }
}