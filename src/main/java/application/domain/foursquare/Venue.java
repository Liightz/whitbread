package application.domain.foursquare;

import java.util.ArrayList;

public class Venue {
    private String id;
    private String name;
    private Contact contact;
    private Location location;
    private ArrayList<Category> categories;
    private boolean verified;
    private Stats stats;
    private String url;
    private double rating;
    private String ratingColor;
    private int ratingSignals;
    private boolean allowMenuUrlEdit;
    private BeenHere beenHere;
    private Hours hours;
    private Events events;
    private Photos photos;
    private String storeId;
    private HereNow hereNow;

    public String getId() { return this.id; }

    public void setId(String id) { this.id = id; }

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }

    public Contact getContact() { return this.contact; }

    public void setContact(Contact contact) { this.contact = contact; }

    public Location getLocation() { return this.location; }

    public void setLocation(Location location) { this.location = location; }

    public ArrayList<Category> getCategories() { return this.categories; }

    public void setCategories(ArrayList<Category> categories) { this.categories = categories; }

    public boolean getVerified() { return this.verified; }

    public void setVerified(boolean verified) { this.verified = verified; }

    public Stats getStats() { return this.stats; }

    public void setStats(Stats stats) { this.stats = stats; }

    public String getUrl() { return this.url; }

    public void setUrl(String url) { this.url = url; }

    public double getRating() { return this.rating; }

    public void setRating(double rating) { this.rating = rating; }

    public String getRatingColor() { return this.ratingColor; }

    public void setRatingColor(String ratingColor) { this.ratingColor = ratingColor; }

    public int getRatingSignals() { return this.ratingSignals; }

    public void setRatingSignals(int ratingSignals) { this.ratingSignals = ratingSignals; }

    public boolean getAllowMenuUrlEdit() { return this.allowMenuUrlEdit; }

    public void setAllowMenuUrlEdit(boolean allowMenuUrlEdit) { this.allowMenuUrlEdit = allowMenuUrlEdit; }

    public BeenHere getBeenHere() { return this.beenHere; }

    public void setBeenHere(BeenHere beenHere) { this.beenHere = beenHere; }

    public Hours getHours() { return this.hours; }

    public void setHours(Hours hours) { this.hours = hours; }

    public Events getEvents() { return this.events; }

    public void setEvents(Events events) { this.events = events; }

    public Photos getPhotos() { return this.photos; }

    public void setPhotos(Photos photos) { this.photos = photos; }

    public String getStoreId() { return this.storeId; }

    public void setStoreId(String storeId) { this.storeId = storeId; }

    public HereNow getHereNow() { return this.hereNow; }

    public void setHereNow(HereNow hereNow) { this.hereNow = hereNow; }
}
