package application.domain.foursquare;

public class LabeledLatLng {
    private String label;
    private double lat;
    private double lng;


    public String getLabel() { return this.label; }

    public void setLabel(String label) { this.label = label; }

    public double getLat() { return this.lat; }

    public void setLat(double lat) { this.lat = lat; }

    public double getLng() { return this.lng; }

    public void setLng(double lng) { this.lng = lng; }
}
