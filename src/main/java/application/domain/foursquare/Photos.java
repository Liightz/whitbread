package application.domain.foursquare;

import java.util.ArrayList;

public class Photos {
    private int count;
    private ArrayList<Object> groups;

    public int getCount() { return this.count; }

    public void setCount(int count) { this.count = count; }

    public ArrayList<Object> getGroups() { return this.groups; }

    public void setGroups(ArrayList<Object> groups) { this.groups = groups; }
}
