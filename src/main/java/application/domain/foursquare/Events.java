package application.domain.foursquare;

public class Events {
    private int count;
    private String summary;

    public int getCount() { return this.count; }

    public void setCount(int count) { this.count = count; }

    public String getSummary() { return this.summary; }

    public void setSummary(String summary) { this.summary = summary; }
}