package application.domain.foursquare;

public class Filter {
    private String name;
    private String key;

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }

    public String getKey() { return this.key; }

    public void setKey(String key) { this.key = key; }
}