package application.domain.foursquare;

public class Tip {
    private String id;
    private int createdAt;
    private String text;
    private String type;
    private String canonicalUrl;
    private Likes likes;
    private boolean logView;
    private int agreeCount;
    private int disagreeCount;
    private Todo todo;
    private User user;

    public String getId() { return this.id; }

    public void setId(String id) { this.id = id; }

    public int getCreatedAt() { return this.createdAt; }

    public void setCreatedAt(int createdAt) { this.createdAt = createdAt; }

    public String getText() { return this.text; }

    public void setText(String text) { this.text = text; }

    public String getType() { return this.type; }

    public void setType(String type) { this.type = type; }

    public String getCanonicalUrl() { return this.canonicalUrl; }

    public void setCanonicalUrl(String canonicalUrl) { this.canonicalUrl = canonicalUrl; }

    public Likes getLikes() { return this.likes; }

    public void setLikes(Likes likes) { this.likes = likes; }

    public boolean getLogView() { return this.logView; }

    public void setLogView(boolean logView) { this.logView = logView; }

    public int getAgreeCount() { return this.agreeCount; }

    public void setAgreeCount(int agreeCount) { this.agreeCount = agreeCount; }

    public int getDisagreeCount() { return this.disagreeCount; }

    public void setDisagreeCount(int disagreeCount) { this.disagreeCount = disagreeCount; }

    public Todo getTodo() { return this.todo; }

    public void setTodo(Todo todo) { this.todo = todo; }

    public User getUser() { return this.user; }

    public void setUser(User user) { this.user = user; }
}
