package application.domain.foursquare;

public class Geometry {
    private Bounds bounds;

    public Bounds getBounds() { return this.bounds; }

    public void setBounds(Bounds bounds) { this.bounds = bounds; }
}