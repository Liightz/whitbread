package application.domain.foursquare;

public class FullResponse {
    private Meta meta;
    private Response response;

    public Meta getMeta() { return this.meta; }

    public void setMeta(Meta meta) { this.meta = meta; }

    public Response getResponse() { return this.response; }

    public void setResponse(Response response) { this.response = response; }
}
