package application.domain.foursquare;

import java.util.ArrayList;

public class Item {
    private Reasons reasons;
    private Venue venue;
    private ArrayList<Tip> tips;
    private String referralId;

    public Reasons getReasons() { return this.reasons; }

    public void setReasons(Reasons reasons) { this.reasons = reasons; }

    public Venue getVenue() { return this.venue; }

    public void setVenue(Venue venue) { this.venue = venue; }

    public ArrayList<Tip> getTips() { return this.tips; }

    public void setTips(ArrayList<Tip> tips) { this.tips = tips; }

    public String getReferralId() { return this.referralId; }

    public void setReferralId(String referralId) { this.referralId = referralId; }
}