package application.domain.foursquare;

public class User {
    private String id;
    private String firstName;
    private String gender;
    private Photo photo;

    public String getId() { return this.id; }

    public void setId(String id) { this.id = id; }

    public String getFirstName() { return this.firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getGender() { return this.gender; }

    public void setGender(String gender) { this.gender = gender; }

    public Photo getPhoto() { return this.photo; }

    public void setPhoto(Photo photo) { this.photo = photo; }
}