package application.domain.foursquare;

public class Hours {
    private String status;
    private boolean isOpen;
    private boolean isLocalHoliday;

    public String getStatus() { return this.status; }

    public void setStatus(String status) { this.status = status; }

    public boolean getIsOpen() { return this.isOpen; }

    public void setIsOpen(boolean isOpen) { this.isOpen = isOpen; }

    public boolean getIsLocalHoliday() { return this.isLocalHoliday; }

    public void setIsLocalHoliday(boolean isLocalHoliday) { this.isLocalHoliday = isLocalHoliday; }
}
