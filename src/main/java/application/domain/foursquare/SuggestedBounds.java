package application.domain.foursquare;

public class SuggestedBounds {
    private Ne ne;
    private Sw sw;

    public Ne getNe() { return this.ne; }

    public void setNe(Ne ne) { this.ne = ne; }

    public Sw getSw() { return this.sw; }

    public void setSw(Sw sw) { this.sw = sw; }
}