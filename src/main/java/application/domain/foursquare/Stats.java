package application.domain.foursquare;

public class Stats {
    private int checkinsCount;
    private int usersCount;
    private int tipCount;


    public int getCheckinsCount() { return this.checkinsCount; }

    public void setCheckinsCount(int checkinsCount) { this.checkinsCount = checkinsCount; }

    public int getUsersCount() { return this.usersCount; }

    public void setUsersCount(int usersCount) { this.usersCount = usersCount; }

    public int getTipCount() { return this.tipCount; }

    public void setTipCount(int tipCount) { this.tipCount = tipCount; }
}