package application.domain.foursquare;

public class BeenHere {
    private int count;
    private boolean marked;
    private int lastCheckinExpiredAt;

    public int getCount() { return this.count; }

    public void setCount(int count) { this.count = count; }

    public boolean getMarked() { return this.marked; }

    public void setMarked(boolean marked) { this.marked = marked; }

    public int getLastCheckinExpiredAt() { return this.lastCheckinExpiredAt; }

    public void setLastCheckinExpiredAt(int lastCheckinExpiredAt) { this.lastCheckinExpiredAt = lastCheckinExpiredAt; }
}
