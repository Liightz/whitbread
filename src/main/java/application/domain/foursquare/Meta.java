package application.domain.foursquare;

public class Meta {
    private int code;
    private String errorDetail;
    private String errorType;
    private String requestId;

    public int getCode() { return this.code; }

    public void setCode(int code) { this.code = code; }

    public String getRequestId() { return this.requestId; }

    public void setRequestId(String requestId) { this.requestId = requestId; }

    public String getErrorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }
}