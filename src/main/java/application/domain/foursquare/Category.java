package application.domain.foursquare;

public class Category {
    private String id;
    private String name;
    private String pluralName;
    private String shortName;
    private Icon icon;
    private boolean primary;

    public String getId() { return this.id; }

    public void setId(String id) { this.id = id; }

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }

    public String getPluralName() { return this.pluralName; }

    public void setPluralName(String pluralName) { this.pluralName = pluralName; }

    public String getShortName() { return this.shortName; }

    public void setShortName(String shortName) { this.shortName = shortName; }

    public Icon getIcon() { return this.icon; }

    public void setIcon(Icon icon) { this.icon = icon; }

    public boolean getPrimary() { return this.primary; }

    public void setPrimary(boolean primary) { this.primary = primary; }
}
