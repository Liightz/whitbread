package application.domain.foursquare;

public class Warning {
    private String text;

    public String getText() { return this.text; }

    public void setText(String text) { this.text = text; }
}
