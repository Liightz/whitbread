package application.domain.foursquare;

import java.util.ArrayList;

public class Likes {
    private int count;
    private ArrayList<Object> groups;
    private String summary;

    public int getCount() { return this.count; }

    public void setCount(int count) { this.count = count; }

    public ArrayList<Object> getGroups() { return this.groups; }

    public void setGroups(ArrayList<Object> groups) { this.groups = groups; }

    public String getSummary() { return this.summary; }

    public void setSummary(String summary) { this.summary = summary; }
}