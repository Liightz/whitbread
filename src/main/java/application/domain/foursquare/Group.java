package application.domain.foursquare;

import java.util.ArrayList;

public class Group {
    private String type;
    private String name;
    private ArrayList<Item> items;

    public String getType() { return this.type; }

    public void setType(String type) { this.type = type; }

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }

    public ArrayList<Item> getItems() { return this.items; }

    public void setItems(ArrayList<Item> items) { this.items = items; }
}
