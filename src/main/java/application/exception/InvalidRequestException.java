package application.exception;

/**
 * @author j000106.
 */
public class InvalidRequestException extends Exception {

    public InvalidRequestException(String message) {
        super(message);
    }
}
