package application.controller;

import application.domain.Request;
import application.domain.foursquare.FullResponse;
import application.service.FourSquareService;
import application.validator.RequestValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class FourSquareController {

    private FourSquareService fourSquareService;
    private RequestValidator requestValidator;

    @Autowired
    public FourSquareController(FourSquareService fourSquareService, RequestValidator requestValidator) {
        this.fourSquareService = fourSquareService;
        this.requestValidator = requestValidator;
    }

    /**
     * Gets recommended places near the location provided, using four square api to get the recommended locations
     * @param location The location that we need recommended places for
     * @return  A Response containing information the recommended locations near the location provided
     * @throws Exception
     */
    @RequestMapping(value = "/recommended/{location}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public FullResponse getRecommendedPlacesNearLocation(@PathVariable(value = "location") String location) throws Exception {
        Request request = new Request(location);
        requestValidator.validate(request);
        return fourSquareService.findRecommendedLocationsNearLocation(request);
    }

    @ExceptionHandler(HttpStatusCodeException.class)
    public FullResponse handleFourSquareApiError(HttpStatusCodeException ex, HttpServletResponse response) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        FullResponse fullResponse = objectMapper.readValue(ex.getResponseBodyAsString(), FullResponse.class);
        response.setStatus(fullResponse.getMeta().getCode());
        return fullResponse;
    }
}
