package application.service;

import application.domain.Request;
import application.domain.foursquare.FullResponse;
import application.generator.FourSquareUrlGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Service
public class FourSquareService {
    private RestTemplate restTemplate;
    private FourSquareUrlGenerator fourSquareUrlGenerator;

    @Autowired
    public FourSquareService(RestTemplate restTemplate, FourSquareUrlGenerator urlGenerator) {
        this.restTemplate = restTemplate;
        this.fourSquareUrlGenerator = urlGenerator;
    }

    public FullResponse findRecommendedLocationsNearLocation(Request request) throws Exception {
        URI url = fourSquareUrlGenerator.generateExploreUrl(request);
        ResponseEntity<FullResponse> reply = restTemplate.getForEntity(url, FullResponse.class);
        return reply.getBody();
    }
}
