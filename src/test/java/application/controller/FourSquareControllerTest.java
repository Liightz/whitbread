package application.controller;

import application.domain.Request;
import application.domain.foursquare.FullResponse;
import application.service.FourSquareService;
import application.validator.RequestValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.charset.Charset;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(MockitoJUnitRunner.class)
public class FourSquareControllerTest {

    @Mock
    private FourSquareService fourSquareService;
    @Mock
    private RequestValidator requestValidator;

    private FullResponse validResponse;
    private String badRequestException;
    private String internalServerErrorException;

    private MockMvc mvc;

    @Before
    public void setup() throws Exception {
        mvc = MockMvcBuilders.standaloneSetup(new FourSquareController(fourSquareService, requestValidator)).build();
        when(requestValidator.validate(any())).thenCallRealMethod();

        String json = "{\"meta\":{\"code\":200,\"requestId\":\"5999e18e6a607176ff497987\"},\"response\":{\"suggestedFilters\":{\"header\":\"Tap to show:\",\"filters\":[{\"name\":\"Open now\",\"key\":\"openNow\"}]},\"geocode\":{\"what\":\"\",\"where\":\"haverhill england\",\"center\":{\"lat\":52.08226,\"lng\":0.43891},\"displayString\":\"Haverhill, Suffolk, United Kingdom\",\"cc\":\"GB\",\"geometry\":{\"bounds\":{\"ne\":{\"lat\":52.103479036138324,\"lng\":0.4540798466918039},\"sw\":{\"lat\":52.06707278971752,\"lng\":0.417673873086726}}},\"slug\":\"haverhill-united-kingdom\",\"longId\":\"72057594040575246\"},\"warning\":{\"text\":\"There aren't a lot of results near you. Try something more general, reset your filters, or expand the search area.\"},\"headerLocation\":\"Haverhill\",\"headerFullLocation\":\"Haverhill\",\"headerLocationGranularity\":\"city\",\"totalResults\":33,\"suggestedBounds\":{\"ne\":{\"lat\":52.082959817254114,\"lng\":0.43750828175001627},\"sw\":{\"lat\":52.080260157468665,\"lng\":0.4486235905257369}},\"groups\":[{\"type\":\"Recommended Places\",\"name\":\"recommended\",\"items\":[{\"reasons\":{\"count\":0,\"items\":[{\"summary\":\"This spot is popular\",\"type\":\"general\",\"reasonName\":\"globalInteractionReason\"}]},\"venue\":{\"id\":\"4ba3e252f964a520f86938e3\",\"name\":\"Cineworld\",\"contact\":{\"phone\":\"+448712002000\",\"formattedPhone\":\"+44 871 200 2000\",\"twitter\":\"cineworld\",\"facebook\":\"110418892318212\",\"facebookUsername\":\"cineworld\",\"facebookName\":\"Cineworld Cinemas\"},\"location\":{\"address\":\"Leisure & Retail Complex\",\"crossStreet\":\"Ehringshausen Way\",\"lat\":52.08160998736139,\"lng\":0.4430659361378766,\"labeledLatLngs\":[{\"label\":\"display\",\"lat\":52.08160998736139,\"lng\":0.4430659361378766}],\"postalCode\":\"CB9 0ER\",\"cc\":\"GB\",\"city\":\"Haverhill\",\"state\":\"Suffolk\",\"country\":\"United Kingdom\",\"formattedAddress\":[\"Leisure & Retail Complex (Ehringshausen Way)\",\"Haverhill\",\"Suffolk\",\"CB9 0ER\",\"United Kingdom\"]},\"categories\":[{\"id\":\"4bf58dd8d48988d17f941735\",\"name\":\"Movie Theater\",\"pluralName\":\"Movie Theaters\",\"shortName\":\"Movie Theater\",\"icon\":{\"prefix\":\"https:\\/\\/ss3.4sqi.net\\/img\\/categories_v2\\/arts_entertainment\\/movietheater_\",\"suffix\":\".png\"},\"primary\":true}],\"verified\":true,\"stats\":{\"checkinsCount\":524,\"usersCount\":115,\"tipCount\":6},\"url\":\"http:\\/\\/www.cineworld.com\",\"rating\":8.4,\"ratingColor\":\"73CF42\",\"ratingSignals\":19,\"allowMenuUrlEdit\":true,\"beenHere\":{\"count\":0,\"marked\":false,\"lastCheckinExpiredAt\":0},\"hours\":{\"status\":\"Likely open\",\"isOpen\":true,\"isLocalHoliday\":false},\"events\":{\"count\":11,\"summary\":\"11 movies\"},\"photos\":{\"count\":0,\"groups\":[]},\"storeId\":\"\",\"hereNow\":{\"count\":0,\"summary\":\"Nobody here\",\"groups\":[]}},\"tips\":[{\"id\":\"4c9b7233971676b0b45f5ce2\",\"createdAt\":1285255731,\"text\":\"Cineworld Haverhill does Bargain Tuesday tickets. Come to the cinema on any Tuesday and get cut-price entry.\",\"type\":\"user\",\"canonicalUrl\":\"https:\\/\\/foursquare.com\\/item\\/4c9b7233971676b0b45f5ce2\",\"likes\":{\"count\":2,\"groups\":[],\"summary\":\"2 likes\"},\"logView\":true,\"agreeCount\":2,\"disagreeCount\":0,\"todo\":{\"count\":0},\"user\":{\"id\":\"3243528\",\"firstName\":\"Cineworld\",\"gender\":\"male\",\"photo\":{\"prefix\":\"https:\\/\\/igx.4sqi.net\\/img\\/user\\/\",\"suffix\":\"\\/QU0CJCUBJCJ2JIQU.jpg\"}}}],\"referralId\":\"e-0-4ba3e252f964a520f86938e3-0\"}]}]}}";
        badRequestException = "{\"meta\":{\"code\":400,\"errorType\":\"invalid_auth\",\"errorDetail\":\"Missing access credentials. See https:\\/\\/developer.foursquare.com\\/docs\\/oauth.html for details.\",\"requestId\":\"59a4484b4c1f6751a7b44714\"},\"response\":{}}";
        internalServerErrorException = "{\"meta\":{\"code\":500,\"errorType\":\"invalid_auth\",\"errorDetail\":\"Missing access credentials. See https:\\/\\/developer.foursquare.com\\/docs\\/oauth.html for details.\",\"requestId\":\"59a4484b4c1f6751a7b44714\"},\"response\":{}}";
        ObjectMapper mapper = new ObjectMapper();
        validResponse = mapper.readValue(json, FullResponse.class);
    }

    @Test
    public void shouldReturnOKWhenGettingRecommendedLocationsForValidLocation() throws Exception {
        when(fourSquareService.findRecommendedLocationsNearLocation(any(Request.class))).thenReturn(validResponse);
        MvcResult result = this.mvc.perform(get("/recommended/London")).andExpect(status().isOk()).andReturn();
    }

    @Test
    public void shouldValidateRequestWhenGettingRecommendedLocation() throws Exception {
        when(fourSquareService.findRecommendedLocationsNearLocation(any(Request.class))).thenReturn(validResponse);
        MvcResult result = this.mvc.perform(get("/recommended/London")).andExpect(status().isOk()).andReturn();
        verify(requestValidator, times(1)).validate(any());
    }

    @Test
    public void shouldReturnBadRequestWhenFourSquareReturnsBadRequest() throws Exception {
        HttpClientErrorException errorException = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Test Exception", badRequestException.getBytes(), Charset.defaultCharset());
        when(fourSquareService.findRecommendedLocationsNearLocation(any(Request.class))).thenThrow(errorException);
        MvcResult result = this.mvc.perform(get("/recommended/London")).andExpect(status().isBadRequest()).andReturn();
    }

    @Test
    public void shouldReturnInternalServerErrorExceptionWhenFourSquareReturnsInternalServerError() throws Exception {
        HttpClientErrorException errorException = new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Test Exception", internalServerErrorException.getBytes(), Charset.defaultCharset());
        when(fourSquareService.findRecommendedLocationsNearLocation(any(Request.class))).thenThrow(errorException);
        MvcResult result = this.mvc.perform(get("/recommended/London")).andExpect(status().isInternalServerError()).andReturn();
    }
}
