package application.service;

import application.domain.Request;
import application.domain.foursquare.FullResponse;
import application.generator.FourSquareUrlGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FourSquareServiceTest {

    @Mock
    private RestTemplate restTemplate;
    @Mock
    private FourSquareUrlGenerator fourSquareUrlGenerator;

    private FourSquareService fourSquareService;

    @Before
    public void setup() throws URISyntaxException {
        this.fourSquareService = new FourSquareService(restTemplate, fourSquareUrlGenerator);
        FullResponse response = new FullResponse();
        ResponseEntity<FullResponse> responseEntity = new ResponseEntity<FullResponse>(response, HttpStatus.OK);
        System.out.println(fourSquareUrlGenerator==null);
        when(fourSquareUrlGenerator.generateExploreUrl(any(Request.class))).thenReturn(new URI("URL"));
        when(restTemplate.getForEntity(any(URI.class), eq(FullResponse.class))).thenReturn(responseEntity);
    }

    @Test
    public void shouldCallFourSquareApiWhenFindingRecommendedLocationsByLocation() throws Exception {
        FullResponse response = fourSquareService.findRecommendedLocationsNearLocation(new Request("location"));
        assertNotNull(response);
        verify(restTemplate, times(1)).getForEntity(any(URI.class), eq(FullResponse.class));
    }
}
