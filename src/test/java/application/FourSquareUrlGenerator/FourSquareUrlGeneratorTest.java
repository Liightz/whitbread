package application.FourSquareUrlGenerator;

import application.config.Properties;
import application.domain.Request;
import application.generator.FourSquareUrlGenerator;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

/**
 * @author j000106.
 */
public class FourSquareUrlGeneratorTest {
    private static final String CLIENT_ID = "clientid";
    private static final String CLIENT_SECRET = "clientsecret";

    private FourSquareUrlGenerator fourSquareUrlGenerator;

    @Before
    public void setup() {
        Properties properties = new Properties();
        properties.setFourSquareClientId(CLIENT_ID);
        properties.setFourSquareClientSecret(CLIENT_SECRET);
        fourSquareUrlGenerator = new FourSquareUrlGenerator(properties);
    }

    @Test
    public void shouldGenerateExploreUrlCorrectly() throws URISyntaxException {
        String expectedUrl = "https://api.foursquare.com/v2/venues/explore?client_id=clientid&client_secret=clientsecret&v=20170827&near=London";
        Request request = new Request("London");
        URI uri = fourSquareUrlGenerator.generateExploreUrl(request);
        assertEquals(expectedUrl, uri.toString());
    }
}
