package application.validation;

import application.domain.Request;
import application.exception.InvalidRequestException;
import application.validator.RequestValidator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author j000106.
 */
public class RequestValidatorTest {

    private RequestValidator validator;

    @Before
    public void setup() {
        this.validator = new RequestValidator();
    }

    @Test
    public void shouldSuccessfullyValidateAValidRequest() throws InvalidRequestException {
        Request validRequest = new Request("London");
        boolean valid = validator.validate(validRequest);
    }

    @Test(expected = InvalidRequestException.class)
    public void shouldFailValidationForRequestWithNullSearchName() throws InvalidRequestException {
        Request invalidNameRequest = new Request(null);
        boolean valid = validator.validate(invalidNameRequest);
    }

    @Test(expected = InvalidRequestException.class)
    public void shouldFailValidationForRequestWithEmptySearchName() throws InvalidRequestException {
        Request invalidNameRequest = new Request("");
        boolean valid = validator.validate(invalidNameRequest);
    }
}
