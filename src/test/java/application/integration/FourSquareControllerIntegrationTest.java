package application.integration;

import application.domain.foursquare.FullResponse;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class FourSquareControllerIntegrationTest {

    private static final ParameterizedTypeReference<FullResponse> responseType = new ParameterizedTypeReference<FullResponse>() {};

    @LocalServerPort
    private int serverPort;

    private String requestUrl;

    @Before
    public void setup() {
        requestUrl = "http://localhost:" + this.serverPort + "/recommended/London";
    }

    /**
     * This test is ignored, as application.properties must have the client id/client secret filled in to make it work
     */
    @Test
    @Ignore
    public void shouldReturnRecommendedLocationsNearLocation() {
        ResponseEntity<FullResponse> response = new TestRestTemplate().exchange(requestUrl, HttpMethod.GET, null, responseType);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        FullResponse body = response.getBody();
        assertEquals(body.getMeta().getCode(), HttpStatus.OK.value());
    }
}
